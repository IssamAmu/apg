package apg;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Programme {

	/**
	 * Programme principal.
	 * @param args Arguments d'entrée pour le programme.
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("Utilisation : java programme [chemin vers le fichier]");
			System.exit(0);
		}
		
		String nomFichier = args[0];
		Donnees donnees = new Donnees();
		ArrayList<Fournisseur> fournisseurs = null;
		try {
			fournisseurs = donnees.readFile(new File(nomFichier));
		} catch (IOException e) {
			System.out.println("Programme lors de la lecture du fichier.");
			e.printStackTrace();
		}
		
		for(Fournisseur fournisseur : fournisseurs) {
			System.out.println("Numéro fournisseur : " + fournisseur.getNumeroFournisseur());
			System.out.println("Coût ouverture : " + fournisseur.getCoutOuverture());
			
			ArrayList<Integer> clients = fournisseur.getClients();
			for(int client : clients) {
				System.out.print("Clients : " + client + "; ");
			}
			System.out.println();
		}
	}
}