package apg;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Classe représentant les données lu d'un fichier.
 * @author issam
 *
 */
public class Donnees {
	public int totalFournisseur;
	public int totalClient;
	
	/**
	 * Lit dans un fichier et extrait les données nécessaires.
	 * @param file Le  fichier.
	 * @return Une <code>ArrayList<Fournisseur></code> contenant tous les fournisseurs.
	 * @throws IOException Exception jeté lors d'une mauvaise lecture du fichier.
	 */
	public ArrayList<Fournisseur> readFile(File file) throws IOException {
		ArrayList<Fournisseur> fournisseurs = new ArrayList<>();
		Scanner scanner = new Scanner(new FileReader(file));
		
		System.out.println("Nom du fichier : " + scanner.nextLine());
		
		totalFournisseur = scanner.nextInt();
		System.out.println("Total fournisseur : " + totalFournisseur);
		
		totalClient = scanner.nextInt();
		System.out.println("Total client : " + totalClient);
		scanner.next(); /* Colonne non utilisée */
		
		for(int fournisseur = 0; fournisseur < totalFournisseur; fournisseur++) {
			int numeroFournisseur = Integer.parseInt(scanner.next());
			int coutOuverture = Integer.parseInt(scanner.next());
			ArrayList<Integer> clients = new ArrayList<>();
			
			for(int client = 0; client < totalClient; client++) {
				clients.add(Integer.parseInt(scanner.next()));
			}
			
			fournisseurs.add(new Fournisseur(numeroFournisseur, coutOuverture, clients));
		}
			
		scanner.close();
		return fournisseurs;
	}
	
	public int getTotalFournisseur() {
		return totalFournisseur;
	}
	
	public int getTotalClient() {
		return totalClient;
	}
}