package apg;
import java.util.ArrayList;

/**
 * Classe représentant un fournisseur.
 * @author issam
 *
 */
public class Fournisseur {
	public int numeroFournisseur;
	public int coutOuverture;
	public ArrayList<Integer> clients;
	
	/**
	 * Constructeur de fournisseurs.
	 * @param numeroFournisseur Numéro du fournisseur.
	 * @param coutOuverture Cout d'ouverture du fournisseur.
	 * @param clients Les clients du fournisseurs.
	 */
	public Fournisseur(int numeroFournisseur, int coutOuverture, ArrayList<Integer> clients) {
		this.numeroFournisseur = numeroFournisseur;
		this.coutOuverture = coutOuverture;
		this.clients = clients;
	}
	
	public int getNumeroFournisseur() {
		return numeroFournisseur;
	}
	
	public int getCoutOuverture() {
		return coutOuverture;
	}
	
	public ArrayList<Integer> getClients() {
		return clients;
	}
}